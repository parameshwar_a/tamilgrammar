# M01
## Meymayakkam Rule 01

## ட் ற் ல் ள் + க ச ப
- When a word contains any of [ட் ற் ல் ள் ] letters then it should be followed by any  of [க ச ப] 
- Not just க ச ப but all their derivatives but not limited to [சி,பு, ... ] can be present too.
### Sample words
- வெட்கம் [ ட் + க]
- வெட்சி [ ட் + சி]
- நுட்பம் [ ட் + ப ]
- கல்பாக்கம் [ல் + பா] 
- கொள்கலம் [ள்  + க]
